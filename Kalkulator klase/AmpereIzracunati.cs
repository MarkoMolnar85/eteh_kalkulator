﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kalkulator_klase
{
    class AmpereIzracunati : Osnovna
    {
        public void VoltOtporPoznato()
        {
            I = V / R;
        }
        public void VatVoltPoznato()
        {
            I = W / V;
        }
        public void VatOtporPoznato()
        {
            I = Math.Sqrt(W / R);
        }
    }
}
