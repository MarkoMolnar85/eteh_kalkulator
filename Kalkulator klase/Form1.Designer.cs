﻿namespace Kalkulator_klase
{
    partial class frmGlavnaForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtwVolt = new System.Windows.Forms.TextBox();
            this.txtwStruja = new System.Windows.Forms.TextBox();
            this.txtwOtpor = new System.Windows.Forms.TextBox();
            this.txtaVolti = new System.Windows.Forms.TextBox();
            this.txtaSnaga = new System.Windows.Forms.TextBox();
            this.txtaOtpor = new System.Windows.Forms.TextBox();
            this.txtvSnaga = new System.Windows.Forms.TextBox();
            this.txtvStruja = new System.Windows.Forms.TextBox();
            this.txtoVolti = new System.Windows.Forms.TextBox();
            this.txtvOtpor = new System.Windows.Forms.TextBox();
            this.txtoStruja = new System.Windows.Forms.TextBox();
            this.txtoSnaga = new System.Windows.Forms.TextBox();
            this.lblNaslovVati = new System.Windows.Forms.Label();
            this.lblNaslovIzracunajAmpere = new System.Windows.Forms.Label();
            this.lblNasloviVolti = new System.Windows.Forms.Label();
            this.lblNaslovOmi = new System.Windows.Forms.Label();
            this.txtwRezultat = new System.Windows.Forms.TextBox();
            this.txtaRezultat = new System.Windows.Forms.TextBox();
            this.txtvRezultat = new System.Windows.Forms.TextBox();
            this.txtoRezultat = new System.Windows.Forms.TextBox();
            this.btnIzracunajVate = new System.Windows.Forms.Button();
            this.btnOcistiw = new System.Windows.Forms.Button();
            this.btnIzracunajAmpere = new System.Windows.Forms.Button();
            this.btnOcistiA = new System.Windows.Forms.Button();
            this.btnIzracunajVolte = new System.Windows.Forms.Button();
            this.btnIzracunajOme = new System.Windows.Forms.Button();
            this.btnOcistiV = new System.Windows.Forms.Button();
            this.btnOcistiO = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cmbIzbor = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // txtwVolt
            // 
            this.txtwVolt.Location = new System.Drawing.Point(22, 66);
            this.txtwVolt.Name = "txtwVolt";
            this.txtwVolt.Size = new System.Drawing.Size(87, 20);
            this.txtwVolt.TabIndex = 0;
            // 
            // txtwStruja
            // 
            this.txtwStruja.Location = new System.Drawing.Point(115, 66);
            this.txtwStruja.Name = "txtwStruja";
            this.txtwStruja.Size = new System.Drawing.Size(87, 20);
            this.txtwStruja.TabIndex = 0;
            // 
            // txtwOtpor
            // 
            this.txtwOtpor.Location = new System.Drawing.Point(208, 66);
            this.txtwOtpor.Name = "txtwOtpor";
            this.txtwOtpor.Size = new System.Drawing.Size(87, 20);
            this.txtwOtpor.TabIndex = 0;
            // 
            // txtaVolti
            // 
            this.txtaVolti.Location = new System.Drawing.Point(377, 66);
            this.txtaVolti.Name = "txtaVolti";
            this.txtaVolti.Size = new System.Drawing.Size(87, 20);
            this.txtaVolti.TabIndex = 0;
            // 
            // txtaSnaga
            // 
            this.txtaSnaga.Location = new System.Drawing.Point(470, 66);
            this.txtaSnaga.Name = "txtaSnaga";
            this.txtaSnaga.Size = new System.Drawing.Size(87, 20);
            this.txtaSnaga.TabIndex = 0;
            // 
            // txtaOtpor
            // 
            this.txtaOtpor.Location = new System.Drawing.Point(563, 66);
            this.txtaOtpor.Name = "txtaOtpor";
            this.txtaOtpor.Size = new System.Drawing.Size(87, 20);
            this.txtaOtpor.TabIndex = 0;
            // 
            // txtvSnaga
            // 
            this.txtvSnaga.Location = new System.Drawing.Point(22, 292);
            this.txtvSnaga.Name = "txtvSnaga";
            this.txtvSnaga.Size = new System.Drawing.Size(87, 20);
            this.txtvSnaga.TabIndex = 0;
            // 
            // txtvStruja
            // 
            this.txtvStruja.Location = new System.Drawing.Point(115, 292);
            this.txtvStruja.Name = "txtvStruja";
            this.txtvStruja.Size = new System.Drawing.Size(87, 20);
            this.txtvStruja.TabIndex = 0;
            // 
            // txtoVolti
            // 
            this.txtoVolti.Location = new System.Drawing.Point(377, 292);
            this.txtoVolti.Name = "txtoVolti";
            this.txtoVolti.Size = new System.Drawing.Size(87, 20);
            this.txtoVolti.TabIndex = 0;
            // 
            // txtvOtpor
            // 
            this.txtvOtpor.Location = new System.Drawing.Point(208, 292);
            this.txtvOtpor.Name = "txtvOtpor";
            this.txtvOtpor.Size = new System.Drawing.Size(87, 20);
            this.txtvOtpor.TabIndex = 0;
            // 
            // txtoStruja
            // 
            this.txtoStruja.Location = new System.Drawing.Point(470, 292);
            this.txtoStruja.Name = "txtoStruja";
            this.txtoStruja.Size = new System.Drawing.Size(87, 20);
            this.txtoStruja.TabIndex = 0;
            // 
            // txtoSnaga
            // 
            this.txtoSnaga.Location = new System.Drawing.Point(563, 292);
            this.txtoSnaga.Name = "txtoSnaga";
            this.txtoSnaga.Size = new System.Drawing.Size(87, 20);
            this.txtoSnaga.TabIndex = 0;
            // 
            // lblNaslovVati
            // 
            this.lblNaslovVati.AutoSize = true;
            this.lblNaslovVati.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaslovVati.Location = new System.Drawing.Point(19, 23);
            this.lblNaslovVati.Name = "lblNaslovVati";
            this.lblNaslovVati.Size = new System.Drawing.Size(98, 18);
            this.lblNaslovVati.TabIndex = 1;
            this.lblNaslovVati.Text = "Izracunaj vate";
            // 
            // lblNaslovIzracunajAmpere
            // 
            this.lblNaslovIzracunajAmpere.AutoSize = true;
            this.lblNaslovIzracunajAmpere.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaslovIzracunajAmpere.Location = new System.Drawing.Point(374, 23);
            this.lblNaslovIzracunajAmpere.Name = "lblNaslovIzracunajAmpere";
            this.lblNaslovIzracunajAmpere.Size = new System.Drawing.Size(121, 18);
            this.lblNaslovIzracunajAmpere.TabIndex = 1;
            this.lblNaslovIzracunajAmpere.Text = "Izracunaj ampere";
            // 
            // lblNasloviVolti
            // 
            this.lblNasloviVolti.AutoSize = true;
            this.lblNasloviVolti.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNasloviVolti.Location = new System.Drawing.Point(19, 245);
            this.lblNasloviVolti.Name = "lblNasloviVolti";
            this.lblNasloviVolti.Size = new System.Drawing.Size(102, 18);
            this.lblNasloviVolti.TabIndex = 1;
            this.lblNasloviVolti.Text = "Izracunaj volte";
            // 
            // lblNaslovOmi
            // 
            this.lblNaslovOmi.AutoSize = true;
            this.lblNaslovOmi.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNaslovOmi.Location = new System.Drawing.Point(374, 245);
            this.lblNaslovOmi.Name = "lblNaslovOmi";
            this.lblNaslovOmi.Size = new System.Drawing.Size(101, 18);
            this.lblNaslovOmi.TabIndex = 1;
            this.lblNaslovOmi.Text = "Izracunaj ome";
            // 
            // txtwRezultat
            // 
            this.txtwRezultat.Location = new System.Drawing.Point(115, 116);
            this.txtwRezultat.Name = "txtwRezultat";
            this.txtwRezultat.Size = new System.Drawing.Size(87, 20);
            this.txtwRezultat.TabIndex = 0;
            // 
            // txtaRezultat
            // 
            this.txtaRezultat.Location = new System.Drawing.Point(470, 116);
            this.txtaRezultat.Name = "txtaRezultat";
            this.txtaRezultat.Size = new System.Drawing.Size(87, 20);
            this.txtaRezultat.TabIndex = 0;
            // 
            // txtvRezultat
            // 
            this.txtvRezultat.Location = new System.Drawing.Point(115, 341);
            this.txtvRezultat.Name = "txtvRezultat";
            this.txtvRezultat.Size = new System.Drawing.Size(87, 20);
            this.txtvRezultat.TabIndex = 0;
            // 
            // txtoRezultat
            // 
            this.txtoRezultat.Location = new System.Drawing.Point(470, 341);
            this.txtoRezultat.Name = "txtoRezultat";
            this.txtoRezultat.Size = new System.Drawing.Size(87, 20);
            this.txtoRezultat.TabIndex = 0;
            // 
            // btnIzracunajVate
            // 
            this.btnIzracunajVate.Location = new System.Drawing.Point(115, 151);
            this.btnIzracunajVate.Name = "btnIzracunajVate";
            this.btnIzracunajVate.Size = new System.Drawing.Size(87, 27);
            this.btnIzracunajVate.TabIndex = 2;
            this.btnIzracunajVate.Text = "Izracunaj vate";
            this.btnIzracunajVate.UseVisualStyleBackColor = true;
            this.btnIzracunajVate.Click += new System.EventHandler(this.btnIzracunajVate_Click);
            // 
            // btnOcistiw
            // 
            this.btnOcistiw.Location = new System.Drawing.Point(115, 184);
            this.btnOcistiw.Name = "btnOcistiw";
            this.btnOcistiw.Size = new System.Drawing.Size(87, 27);
            this.btnOcistiw.TabIndex = 2;
            this.btnOcistiw.Text = "Ocisti";
            this.btnOcistiw.UseVisualStyleBackColor = true;
            this.btnOcistiw.Click += new System.EventHandler(this.btnOcistiw_Click);
            // 
            // btnIzracunajAmpere
            // 
            this.btnIzracunajAmpere.Location = new System.Drawing.Point(470, 151);
            this.btnIzracunajAmpere.Name = "btnIzracunajAmpere";
            this.btnIzracunajAmpere.Size = new System.Drawing.Size(87, 27);
            this.btnIzracunajAmpere.TabIndex = 2;
            this.btnIzracunajAmpere.Text = "Izracunaj ampere";
            this.btnIzracunajAmpere.UseVisualStyleBackColor = true;
            this.btnIzracunajAmpere.Click += new System.EventHandler(this.btnIzracunajAmpere_Click);
            // 
            // btnOcistiA
            // 
            this.btnOcistiA.Location = new System.Drawing.Point(470, 184);
            this.btnOcistiA.Name = "btnOcistiA";
            this.btnOcistiA.Size = new System.Drawing.Size(87, 27);
            this.btnOcistiA.TabIndex = 2;
            this.btnOcistiA.Text = "Ocisti";
            this.btnOcistiA.UseVisualStyleBackColor = true;
            this.btnOcistiA.Click += new System.EventHandler(this.btnOcistiA_Click);
            // 
            // btnIzracunajVolte
            // 
            this.btnIzracunajVolte.Location = new System.Drawing.Point(115, 367);
            this.btnIzracunajVolte.Name = "btnIzracunajVolte";
            this.btnIzracunajVolte.Size = new System.Drawing.Size(87, 27);
            this.btnIzracunajVolte.TabIndex = 2;
            this.btnIzracunajVolte.Text = "Izracunaj volte";
            this.btnIzracunajVolte.UseVisualStyleBackColor = true;
            this.btnIzracunajVolte.Click += new System.EventHandler(this.btnIzracunajVolte_Click);
            // 
            // btnIzracunajOme
            // 
            this.btnIzracunajOme.Location = new System.Drawing.Point(470, 367);
            this.btnIzracunajOme.Name = "btnIzracunajOme";
            this.btnIzracunajOme.Size = new System.Drawing.Size(87, 27);
            this.btnIzracunajOme.TabIndex = 2;
            this.btnIzracunajOme.Text = "Izracunaj ome";
            this.btnIzracunajOme.UseVisualStyleBackColor = true;
            this.btnIzracunajOme.Click += new System.EventHandler(this.btnIzracunajOme_Click);
            // 
            // btnOcistiV
            // 
            this.btnOcistiV.Location = new System.Drawing.Point(115, 400);
            this.btnOcistiV.Name = "btnOcistiV";
            this.btnOcistiV.Size = new System.Drawing.Size(87, 27);
            this.btnOcistiV.TabIndex = 2;
            this.btnOcistiV.Text = "Ocisti";
            this.btnOcistiV.UseVisualStyleBackColor = true;
            this.btnOcistiV.Click += new System.EventHandler(this.btnOcistiV_Click);
            // 
            // btnOcistiO
            // 
            this.btnOcistiO.Location = new System.Drawing.Point(470, 400);
            this.btnOcistiO.Name = "btnOcistiO";
            this.btnOcistiO.Size = new System.Drawing.Size(87, 27);
            this.btnOcistiO.TabIndex = 2;
            this.btnOcistiO.Text = "Ocisti";
            this.btnOcistiO.UseVisualStyleBackColor = true;
            this.btnOcistiO.Click += new System.EventHandler(this.btnOcistiO_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label1.Location = new System.Drawing.Point(19, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Volti";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label2.Location = new System.Drawing.Point(112, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Struja";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label3.Location = new System.Drawing.Point(205, 89);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Otpor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label4.Location = new System.Drawing.Point(374, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Volti";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label5.Location = new System.Drawing.Point(467, 89);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Snaga";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label6.Location = new System.Drawing.Point(560, 89);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(33, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Otpor";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label7.Location = new System.Drawing.Point(19, 315);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Snaga";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label8.Location = new System.Drawing.Point(112, 315);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(34, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Struja";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label9.Location = new System.Drawing.Point(377, 315);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Volti";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label10.Location = new System.Drawing.Point(205, 315);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "Otpor";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label11.Location = new System.Drawing.Point(470, 315);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Struja";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label12.Location = new System.Drawing.Point(563, 315);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Snaga";
            // 
            // cmbIzbor
            // 
            this.cmbIzbor.FormattingEnabled = true;
            this.cmbIzbor.Location = new System.Drawing.Point(168, 24);
            this.cmbIzbor.Name = "cmbIzbor";
            this.cmbIzbor.Size = new System.Drawing.Size(127, 21);
            this.cmbIzbor.TabIndex = 4;
            // 
            // frmGlavnaForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(777, 497);
            this.Controls.Add(this.cmbIzbor);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnOcistiO);
            this.Controls.Add(this.btnOcistiV);
            this.Controls.Add(this.btnOcistiA);
            this.Controls.Add(this.btnIzracunajOme);
            this.Controls.Add(this.btnOcistiw);
            this.Controls.Add(this.btnIzracunajVolte);
            this.Controls.Add(this.btnIzracunajAmpere);
            this.Controls.Add(this.btnIzracunajVate);
            this.Controls.Add(this.lblNaslovOmi);
            this.Controls.Add(this.lblNaslovIzracunajAmpere);
            this.Controls.Add(this.lblNasloviVolti);
            this.Controls.Add(this.lblNaslovVati);
            this.Controls.Add(this.txtoSnaga);
            this.Controls.Add(this.txtaOtpor);
            this.Controls.Add(this.txtoRezultat);
            this.Controls.Add(this.txtoStruja);
            this.Controls.Add(this.txtvOtpor);
            this.Controls.Add(this.txtaRezultat);
            this.Controls.Add(this.txtaSnaga);
            this.Controls.Add(this.txtoVolti);
            this.Controls.Add(this.txtwOtpor);
            this.Controls.Add(this.txtvRezultat);
            this.Controls.Add(this.txtvStruja);
            this.Controls.Add(this.txtaVolti);
            this.Controls.Add(this.txtvSnaga);
            this.Controls.Add(this.txtwStruja);
            this.Controls.Add(this.txtwRezultat);
            this.Controls.Add(this.txtwVolt);
            this.Name = "frmGlavnaForma";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtwVolt;
        private System.Windows.Forms.TextBox txtwStruja;
        private System.Windows.Forms.TextBox txtwOtpor;
        private System.Windows.Forms.TextBox txtaVolti;
        private System.Windows.Forms.TextBox txtaSnaga;
        private System.Windows.Forms.TextBox txtaOtpor;
        private System.Windows.Forms.TextBox txtvSnaga;
        private System.Windows.Forms.TextBox txtvStruja;
        private System.Windows.Forms.TextBox txtoVolti;
        private System.Windows.Forms.TextBox txtvOtpor;
        private System.Windows.Forms.TextBox txtoStruja;
        private System.Windows.Forms.TextBox txtoSnaga;
        private System.Windows.Forms.Label lblNaslovVati;
        private System.Windows.Forms.Label lblNaslovIzracunajAmpere;
        private System.Windows.Forms.Label lblNasloviVolti;
        private System.Windows.Forms.Label lblNaslovOmi;
        private System.Windows.Forms.TextBox txtwRezultat;
        private System.Windows.Forms.TextBox txtaRezultat;
        private System.Windows.Forms.TextBox txtvRezultat;
        private System.Windows.Forms.TextBox txtoRezultat;
        private System.Windows.Forms.Button btnIzracunajVate;
        private System.Windows.Forms.Button btnOcistiw;
        private System.Windows.Forms.Button btnIzracunajAmpere;
        private System.Windows.Forms.Button btnOcistiA;
        private System.Windows.Forms.Button btnIzracunajVolte;
        private System.Windows.Forms.Button btnIzracunajOme;
        private System.Windows.Forms.Button btnOcistiV;
        private System.Windows.Forms.Button btnOcistiO;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmbIzbor;
    }
}

