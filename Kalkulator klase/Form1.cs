﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Kalkulator_klase
{
    public partial class frmGlavnaForma : Form
    {
        public frmGlavnaForma()
        {
            InitializeComponent();
            cmbIzbor.Items.Add("Efektivna vrednost");
            cmbIzbor.Items.Add("Amplituda");
            cmbIzbor.Text = "Efektivna vrednost";
        }

        private void btnIzracunajVate_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbIzbor.Text == "Efektivna vrednost")
                {
                    SnaguIzracunati snagaizracunati = new SnaguIzracunati();
                    string v, i, r;
                    v = txtwVolt.Text;
                    i = txtwStruja.Text;
                    r = txtwOtpor.Text;
                    if ((v == "" && i == "" && r == "") || (v == "" && i == "") || (v == "" && r == "") || (i == "" && r == ""))
                    {
                        MessageBox.Show("Potrebno je uneti podatke");
                    }
                    else if (v != "" && i != "")
                    {
                        snagaizracunati.V = double.Parse(v);
                        snagaizracunati.I = double.Parse(i);
                        snagaizracunati.VoltStrujaPoznato();
                        txtwRezultat.Text = snagaizracunati.W.ToString();
                    }
                    else if (i != "" && r != "")
                    {
                        snagaizracunati.I = double.Parse(i);
                        snagaizracunati.R = double.Parse(r);
                        snagaizracunati.StrujaOtporPoznato();
                        txtwRezultat.Text = snagaizracunati.W.ToString();
                    }
                    else if (v != "" && r != "")
                    {
                        snagaizracunati.V = double.Parse(v);
                        snagaizracunati.R = double.Parse(r);
                        snagaizracunati.VoltOtporPoznato();
                        txtwRezultat.Text = snagaizracunati.W.ToString();
                    }
                }
                else if (cmbIzbor.Text == "Amplituda")
                {
                    SnaguIzracunati snagaizracunati = new ZaAplitudu();
                    string v, i, r;
                    v = txtwVolt.Text;
                    i = txtwStruja.Text;
                    r = txtwOtpor.Text;
                    if ((v == "" && i == "" && r == "") || (v == "" && i == "") || (v == "" && r == "") || (i == "" && r == ""))
                    {
                        MessageBox.Show("Potrebno je uneti podatke");
                    }
                    else if (v != "" && i != "")
                    {
                        snagaizracunati.V = double.Parse(v);
                        snagaizracunati.I = double.Parse(i);
                        snagaizracunati.VoltStrujaPoznato();
                        txtwRezultat.Text = snagaizracunati.W.ToString();
                    }
                    else if (i != "" && r != "")
                    {
                        snagaizracunati.I = double.Parse(i);
                        snagaizracunati.R = double.Parse(r);
                        snagaizracunati.StrujaOtporPoznato();
                        txtwRezultat.Text = snagaizracunati.W.ToString();
                    }
                    else if (v != "" && r != "")
                    {
                        snagaizracunati.V = double.Parse(v);
                        snagaizracunati.R = double.Parse(r);
                        snagaizracunati.VoltOtporPoznato();
                        txtwRezultat.Text = snagaizracunati.W.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                txtwRezultat.Text = ex.Message;
            }
        }

        private void btnIzracunajAmpere_Click(object sender, EventArgs e)
        {
            try
            {
                AmpereIzracunati ampIzracunati = new AmpereIzracunati();
                string v, w, r;
                v = txtaVolti.Text; w = txtaSnaga.Text; r = txtaOtpor.Text;
                if ((v == "" && w == "" && r == "") || (v == "" && w == "") || (v == "" && r == "") || (w == "" && r == ""))
                {
                    MessageBox.Show("Potrebno je uneti podatke");
                }
                else if (v != "" && r != "")
                {
                    ampIzracunati.V = double.Parse(v);
                    ampIzracunati.R = double.Parse(r);
                    ampIzracunati.VoltOtporPoznato();
                    txtaRezultat.Text = ampIzracunati.I.ToString();
                }
                else if(w != "" && v != "")
                {
                    ampIzracunati.W = double.Parse(w);
                    ampIzracunati.V = double.Parse(v);
                    ampIzracunati.VatVoltPoznato();
                    txtaRezultat.Text = ampIzracunati.I.ToString();
                }
                else if (w != "" && r != "")
                {
                    ampIzracunati.W = double.Parse(w);
                    ampIzracunati.R = double.Parse(r);
                    ampIzracunati.VatOtporPoznato();
                    txtaRezultat.Text = ampIzracunati.I.ToString();
                }
            }
            catch (Exception ex)
            {
                txtaRezultat.Text = ex.Message;
            }
        }

        private void btnIzracunajVolte_Click(object sender, EventArgs e)
        {
            try
            {
                voltIzracunati voltIzracunaj = new voltIzracunati();
                string i, w, r;
                i = txtvStruja.Text ; w = txtvSnaga.Text; r = txtvOtpor.Text;
                if ((i == "" && w == "" && r == "") || (i == "" && w == "") || (i == "" && r == "") || (w == "" && r == ""))
                {
                    MessageBox.Show("Potrebno je uneti podatke");
                }
                else if(i != "" && r != "")
                {
                    voltIzracunaj.I = double.Parse(i);
                    voltIzracunaj.R = double.Parse(r);
                    voltIzracunaj.StrujaOtporPoznato();
                    txtvRezultat.Text = voltIzracunaj.V.ToString();
                }
                else if (w != "" && r != "")
                {
                    voltIzracunaj.W = double.Parse(w);
                    voltIzracunaj.R = double.Parse(r);
                    voltIzracunaj.VatOtporPoznato();
                    txtvRezultat.Text = voltIzracunaj.V.ToString();
                }
                else if (i != "" && w != "")
                {
                    voltIzracunaj.I = double.Parse(i);
                    voltIzracunaj.W = double.Parse(w);
                    voltIzracunaj.VatStrujaPoznato();
                    txtvRezultat.Text = voltIzracunaj.V.ToString();
                }
            }
            catch (Exception ex)
            {
                txtvRezultat.Text = ex.Message;
            }
        }

        private void btnIzracunajOme_Click(object sender, EventArgs e)
        {
            try
            {
                OmeIzracunati omeIzracun = new OmeIzracunati();
                string v, w, i;
                v = txtoVolti.Text; w = txtoSnaga.Text; i = txtoStruja.Text;
                if ((v == "" && w == "" && i == "") || (v == "" && w == "") || (v == "" && i == "") || (w == "" && i == ""))
                {
                    MessageBox.Show("Potrebno je uneti podatke");
                }
                else if (v != "" && i != "")
                {
                    omeIzracun.V = double.Parse(v);
                    omeIzracun.I = double.Parse(i);
                    omeIzracun.VoltAmperPoznato();
                    txtoRezultat.Text = omeIzracun.R.ToString();
                }
                else if (w != "" && v != "")
                {
                    omeIzracun.W = double.Parse(w);
                    omeIzracun.V = double.Parse(v);
                    omeIzracun.VoltVatPoznato();
                    txtoRezultat.Text = omeIzracun.R.ToString();
                }
                else if (w != "" && i != "")
                {
                    omeIzracun.W = double.Parse(w);
                    omeIzracun.I = double.Parse(i);
                    omeIzracun.VatStrujaPoznato();
                    txtoRezultat.Text = omeIzracun.R.ToString();
                }
            }
            catch (Exception ex)
            {
                txtoRezultat.Text = ex.Message;
            }
        }

        private void btnOcistiw_Click(object sender, EventArgs e)
        {
            txtwOtpor.Text = txtwRezultat.Text = txtwStruja.Text = txtwVolt.Text = "";
            
        }

        private void btnOcistiA_Click(object sender, EventArgs e)
        {
            txtaOtpor.Text = txtaRezultat.Text = txtaSnaga.Text = txtaVolti.Text = "";
        }

        private void btnOcistiV_Click(object sender, EventArgs e)
        {
            txtvOtpor.Text = txtvRezultat.Text = txtvSnaga.Text = txtvStruja.Text = "";
        }

        private void btnOcistiO_Click(object sender, EventArgs e)
        {
            txtoRezultat.Text = txtoSnaga.Text = txtoStruja.Text = txtoVolti.Text = "";
        }

        
        
            
    }
        
}
