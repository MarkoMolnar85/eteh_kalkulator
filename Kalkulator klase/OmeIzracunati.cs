﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kalkulator_klase
{
    class OmeIzracunati : Osnovna
    {
        public void VoltAmperPoznato()
        {
            R = V / I;
        }
        public void VoltVatPoznato()
        {
            R = V * V / W;
        }
        public void VatStrujaPoznato()
        {
            R = W / (I * I);
        }

    }
}
