﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kalkulator_klase
{
    class Osnovna
    {
        public double W { get; set; }       //automatska svojstva
        public double V { get; set; }
        public double I { get; set; }
        public double R { get; set; }
        public Osnovna()
        {
            W = 0;
            V = 0;
            I = 0;
            R = 0;
        }

    }
}
