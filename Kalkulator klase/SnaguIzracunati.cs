﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kalkulator_klase
{
    class SnaguIzracunati : Osnovna
    {
        public virtual void VoltStrujaPoznato()
        {
            W = V * I;
        }
        public virtual void StrujaOtporPoznato()
        {
            W = I * I * R;
        }
        public virtual void VoltOtporPoznato()
        {
            W = V * V / R;
        }
        
    }
}
