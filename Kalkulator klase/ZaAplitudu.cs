﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kalkulator_klase
{
    class ZaAplitudu : SnaguIzracunati
    {
        public override void VoltStrujaPoznato()
        {
            base.VoltStrujaPoznato();
            W *= Math.Sqrt(2);
        }
        public override void StrujaOtporPoznato()
        {
            base.StrujaOtporPoznato();
            W *= Math.Sqrt(2);
        }
        public override void VoltOtporPoznato()
        {
            base.VoltOtporPoznato();
            W *= Math.Sqrt(2);
        }
    }
}
